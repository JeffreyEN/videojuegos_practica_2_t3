package com.xxjshenxx.practica2_t3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.xxjshenxx.practica2_t3.entity.Repositorio;
import com.xxjshenxx.practica2_t3.services.BeeceptorServices;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView rv =findViewById(R.id.rvNames);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://temp-p2-t3.free.beeceptor.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        BeeceptorServices services = retrofit.create(BeeceptorServices.class);
        services.allRepo().enqueue(new Callback<List<Repositorio>>() {
            @Override
            public void onResponse(Call<List<Repositorio>> call, Response<List<Repositorio>> response) {
                RepositoryAdapter adapter = new RepositoryAdapter(response.body());
                rv.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<List<Repositorio>> call, Throwable t) {

            }

            });

    }
}