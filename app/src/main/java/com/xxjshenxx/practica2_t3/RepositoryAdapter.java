package com.xxjshenxx.practica2_t3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import com.squareup.picasso.Picasso;
import com.xxjshenxx.practica2_t3.entity.Repositorio;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.NameViewHolder> {

    private List<Repositorio> data;

    public  RepositoryAdapter(List<Repositorio> data) {
        this.data = data;
    }



    @NonNull
    @Override
    public NameViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item, parent, false);
        return new NameViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RepositoryAdapter.NameViewHolder holder, int i) {
        TextView tvName = holder.itemView.findViewById(R.id.tvname);
        TextView tvSinopsis = holder.itemView.findViewById(R.id.tvsinopsis);
        ImageView imganime = holder.itemView.findViewById(R.id.imganime);
        Repositorio Repo = data.get(i);
        tvName.setText(Repo.getName());
        tvSinopsis.setText(Repo.getSinopsis());

        Picasso.get().load(Repo.getImagen())
                .resize(400,700)
                .into(imganime);



    }

    @Override
    public int getItemCount() {

        return data.size();
    }

    class NameViewHolder extends RecyclerView.ViewHolder {

        public NameViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
