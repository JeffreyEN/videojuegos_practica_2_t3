package com.xxjshenxx.practica2_t3.entity;

public class Repositorio {

    private String name;
    private String sinopsis;
    private String imagen;

    public Repositorio(String name, String sinopsis, String imagen) {
        this.name = name;
        this.sinopsis = sinopsis;
        this.imagen = imagen;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
