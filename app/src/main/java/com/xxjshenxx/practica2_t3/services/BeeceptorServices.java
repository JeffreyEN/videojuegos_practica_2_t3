package com.xxjshenxx.practica2_t3.services;

import com.xxjshenxx.practica2_t3.entity.Repositorio;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface BeeceptorServices {
    @GET("users")
    Call<List<Repositorio>> allRepo();
}
